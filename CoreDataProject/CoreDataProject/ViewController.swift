//
//  ViewController.swift
//  CoreDataProject
//
//  Created by Daniel Freire on 23/1/18.
//  Copyright © 2018 Daniel Freire. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {

    // MARK :- Outlets
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var addressTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    
    let manageObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    var singlePerson: Person?
    var peopleArray: [Person] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func savePerson() {
        let entityDescription = NSEntityDescription.entity(forEntityName: "Person", in: manageObjectContext)
        let person = Person(entity: entityDescription!, insertInto: manageObjectContext)
        
        person.name = nameTextField.text ?? ""
        person.address = addressTextField.text ?? ""
        person.phone = phoneTextField.text ?? ""
        
        do {
            try manageObjectContext.save()
            clearFields()
        } catch {
            print("Error")
        }
    }
    
    func clearFields() {
        nameTextField.text = ""
        addressTextField.text = ""
        phoneTextField.text = ""
    }
    
    func findAll() {
        let request: NSFetchRequest<Person> = Person.fetchRequest()
        
        do {
            let results = try manageObjectContext.fetch(request as! NSFetchRequest<NSFetchRequestResult>)
            for result in results {
                let person = result as! Person
                peopleArray.append(person)
                print("Name \(person.name ?? "") ", terminator: "")
                print("Address \(person.address ?? "") ", terminator: "")
                print("Phone \(person.phone ?? "") ", terminator: "")
                print()
                print()
            }
            performSegue(withIdentifier: "findAllSegue", sender: self)
        } catch {
            print("Error finding ppl")
        }
    }
    
    func findOne() {
        let entityDescription = NSEntityDescription.entity(forEntityName: "Person", in: manageObjectContext)
        let request: NSFetchRequest<Person> = Person.fetchRequest()
        request.entity = entityDescription
        let predicate = NSPredicate(format: "name = %@", nameTextField.text!)
        request.predicate = predicate
        
        do {
            let results = try manageObjectContext.fetch(request as! NSFetchRequest<NSFetchRequestResult>)
            
            if results.count > 0 {
                let match = results[0] as! Person
                singlePerson = match
                performSegue(withIdentifier: "findOneSegue", sender: self)
//                nameTextField.text = match.name
//                addressTextField.text = match.address
//                phoneTextField.text = match.phone
            } else {
                nameTextField.text = "n/a"
                addressTextField.text = "n/a"
                phoneTextField.text = "n/a"
            }
        } catch {
            print("Error finding one")
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "findOneSegue" {
            
            let destination = segue.destination as! FindOneViewController
            destination.person = singlePerson
        }
        
        if segue.identifier == "findAllSegue" {
            
            let destination = segue.destination as! PeopleViewController
            destination.peopleArray = peopleArray
        }
    }
    
    // MARK :- Actions
    @IBAction func saveButtonPressed(_ sender: UIButton) {
        savePerson()
    }
    
    @IBAction func findButtonPressed(_ sender: UIButton) {
        if nameTextField.text == "" {
            findAll()
            return
        }
        findOne()
    }
}

