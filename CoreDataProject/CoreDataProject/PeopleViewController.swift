//
//  PeopleViewController.swift
//  CoreDataProject
//
//  Created by Daniel Freire on 30/1/18.
//  Copyright © 2018 Daniel Freire. All rights reserved.
//

import UIKit

class PeopleViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var peopleTableView: UITableView!
    
    var peopleArray: [Person] = []
    var personIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(peopleArray)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(peopleArray.count)
        return peopleArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "personCell") as! PeopleTableViewCell
        cell.fillData(person: peopleArray[indexPath.row])
        print(peopleArray[indexPath.row])
        //        cell.textLabel?.text = pokemonArray[indexPath.row].name
        return cell
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        personIndex = indexPath.row
        return indexPath
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return "Section 1"
        default:
            return "Section 2"
        }
    }
}
