//
//  FindOneViewController.swift
//  CoreDataProject
//
//  Created by Daniel Freire on 30/1/18.
//  Copyright © 2018 Daniel Freire. All rights reserved.
//

import UIKit
import CoreData

class FindOneViewController: UIViewController {

    // MARK :- Outlets
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    
    var person: Person?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = person?.name
        addressLabel.text = person?.address!
        phoneLabel.text = person?.phone
    }
    
    // MARK :- Actions
    @IBAction func deleteButtonPressed(_ sender: UIButton) {
        
        let manageObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        
        manageObjectContext.delete(person!)
        
        do {
            try manageObjectContext.save()
        } catch {
            print("Error deleting object")
        }
    }
}
