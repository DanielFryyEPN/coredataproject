//
//  PeopleTableViewCell.swift
//  CoreDataProject
//
//  Created by Daniel Freire on 30/1/18.
//  Copyright © 2018 Daniel Freire. All rights reserved.
//

import UIKit

class PeopleTableViewCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func fillData(person: Person) {
        nameLabel.text = person.name
        addressLabel.text = person.address
        phoneLabel.text = person.phone
//        nameLabel.text = pokemon.name
//        heightLabel.text = "\(pokemon.height ?? 0)"
//        weightLabel.text = "\(pokemon.weight ?? 0)"
    }
}
